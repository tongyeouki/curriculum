import dayjs from "dayjs"
import customParseFormat from "dayjs/plugin/customParseFormat"
import { ciep, consulate, cybersecurity, eig, mandarin, synbird, teacher, openData } from "./vars/experiences"
import { Header } from "./components/Header"
import { Topic } from "./components/Experience"
import { Education } from "./components/Education"
import { Tools } from "./components/Tool"
import "./assets/App.css"
import { Job } from "./components/Job"

dayjs.extend(customParseFormat)

function App() {
  const formattedDate = dayjs(dayjs()).format("YYYY-MM-DD")

  return (
    <div className="App">
      <title>{formattedDate}-paul-moysan-curriculum</title>
      <div className="container">
        <div className="content">
          <Header />
          <Topic name="Ouverture des données" dates="depuis 2021">
            <Job {...openData} />
          </Topic>
          <Topic name="Ingénierie logicielle">
            <Job {...eig} />
            <Job {...synbird} />
          </Topic>
          <Topic name="Protection des données" dates="2014-2021">
            <Job {...cybersecurity} />
          </Topic>
          <Topic name="Interculturel" dates="2009-2014">
            <Job {...ciep} />
            <Job {...mandarin} />
            <Job {...teacher} />
            <Job {...consulate} />
          </Topic>
          <Topic name="Formation">
            <Education />
          </Topic>
          <Topic name="Outils">
            <Tools />
          </Topic>
        </div>
      </div>
    </div>
  )
}

export default App
