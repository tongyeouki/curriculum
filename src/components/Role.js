export const Role = ({ name, achievements }) => {
  return (
    <div className="line role">
      {name ? <h4 className="label">{name}</h4> : null}
      <div>
        <ul>
          {achievements.map((achievement, index) => {
            return <li key={index}>{achievement}</li>
          })}
        </ul>
      </div>
    </div>
  )
}
