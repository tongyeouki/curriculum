const education = [
  {
    dates: "2012-2013",
    label: "Études chinoises",
    comment: "HSK4 - équivalent CECR B2",
    place: "Chinese Culture University, Taipei"
  },
  {
    dates: "2011-2012",
    label: "M1 de sociolinguistique et didactique des langues",
    comment: "",
    place: "Université François Rabelais, Tours"
  },
  {
    dates: "2008-2009",
    label: "M2 de Sciences politiques, géopolitique et études européennes",
    comment: "",
    place: "Université Paris-Est"
  },
  {
    dates: "2004-2008",
    label: "Licence de philosophie puis M1 d'éthique et de philosophie politique",
    comment: "",
    place: "Université Paris IV-Sorbonne"
  },
  // {
  //   dates: "2007-2008",
  //   label: "M1 d'Éthique et de philosophie politique",
  //   comment: "",
  //   place: "Université Paris IV-Sorbonne"
  // },
  // {
  //   dates: "2004-2007",
  //   label: "Licence de philosophie",
  //   comment: "",
  //   place: "Université Paris IV-Sorbonne"
  // },
  {
    dates: "2003-2004",
    label: "Hypokhâgne",
    comment: "",
    place: "Lycée Lakanal"
  }
]

export const Education = () => {
  return (
    <div>
      {education.map((item) => {
        return (
          <div key={item.label} className="line" style={{ justifyContent: "space-between" }}>
            <div className="label studies">{item.label}</div>
            <div className="">{item.place}</div>
            <div className="">{item.dates}</div>
          </div>
        )
      })}
    </div>
  )
}
