import { Link } from "./Link"
import { Role } from "./Role"

export const Job = ({ title, subtitle, dates, places, roles }) => {
  return (
    <div className="job">
      <div className="job-designation">
        <div className="job-description">
          <div className="line job-label">
            <h3>{title}</h3>
            <div>{dates}</div>
          </div>
          <div className="line job-label">
            <div className="places">
              {places.map((place) => {
                return (
                  <h4 key={place.name}>
                    <Link link={place.website} title={`${place.name}, ${place.place}`} />
                  </h4>
                )
              })}
            </div>
            <div>
              <i>{subtitle}</i>
            </div>
          </div>
        </div>
      </div>
      <div className="roles">
        {/*<div>{dates}</div>*/}
        {roles.map((r) => {
          return <Role key={r.name} name={r.name} achievements={r.achievements} />
        })}
      </div>
    </div>
  )
}
