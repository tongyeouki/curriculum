import { Skills } from "./Skills"
import { Link } from "./Link"
import photo from "../assets/photo.png"

export const Header = () => {
  return (
    <div className="header">
      <Details />
      <div className="header-center">
        <Catchphrase />
        <Skills />
      </div>
      <img src={photo} height={130} className="photo" alt="Logo" />
    </div>
  )
}

const Details = () => {
  return (
    <>
      <div className="details">
        <div className="details">
          <h1 className="name">Paul Moysan</h1>
        </div>
        <div className="contact">
          <Link link="https://paulmoysan.io" title="https://paulmoysan.io" />
        </div>
        <div className="contact">
          <Link link="mailto: contact@paulmoysan.io" title="contact@paulmoysan.io" />
        </div>
        <div className="contact">
          <Link link="https://gitlab.com/tongyeouki" title="https://gitlab.com/tongyeouki" />
        </div>
      </div>
    </>
  )
}

const Catchphrase = () => {
  return (
    <div className="catchphrase">
      <div>Des données de qualité pour une prise de décision éclairée</div>
      {/*<div>Une approche produit centrée sur la donnée</div>*/}
      {/*<div>Une approche technique orientée utilisateur</div>*/}
    </div>
  )
}
