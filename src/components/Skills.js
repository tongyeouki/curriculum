const skills = [
  // "Enseignement",
  // "Mandarin",
  "Gestion de produit",
  "Ingénierie logicielle",
  "Cybersécurité",
]

export const Skills = () => {
  return (
    <div className="skills">
      {skills.map((skill) => {
        return <div>{skill}</div>
      })}
    </div>
  )
}
