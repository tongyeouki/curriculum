export const Link = ({ link, title }) => (
  <a href={link} target="_blank" rel="noreferrer">
    {title}
  </a>
)
