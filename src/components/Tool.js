const tools = [
  { title: "Langues", content: "Anglais équivalent C1, Mandarin équivalent B1" },
  { title: "Informatique", content: "Kubernetes, Python 3.8+, NodeJs, ReactJs, Bash, Jenkins, Prometheus, Grafana" },
  { title: "Cultures", content: "Approches agiles, approches produit, ingénierie logicielle, cybersécurité" },
  // { title: "Projets", content: "Sites statiques, plateforme de numismatique mérovingienne, time-box.me" },
  {
    title: "Engagements",
    content: "Enfants du Mékong (Thaïlande, été 2007), IHEDN (62e session jeunes, Marly-le-Roi, 2009)"
  }
]

export const Tools = () => {
  return (
    <div>
      {tools.map((tool) => {
        return <Tool title={tool.title} content={tool.content} />
      })}
    </div>
  )
}

const Tool = ({ title, content }) => {
  return (
    <>
      <div className="line">
        <h4 className="label">{title}</h4>
        <div style={{ marginLeft: "15px" }}>{content}</div>
      </div>
    </>
  )
}
