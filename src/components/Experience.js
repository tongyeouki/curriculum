export const Topic = ({ name, dates, children }) => {
  return (
    <div className="topic">
      <div className="topic-line">
        <h2>{name}</h2>
        {/*{dates ? <div className="topic-date">{dates}</div> : null}*/}
      </div>
      <div className="topic-content">{children}</div>
    </div>
  )
}
