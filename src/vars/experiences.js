import {
  allianceFrancaisePlace,
  mefsin,
  dongHuaDaXuePlace,
  bercyHubPlace,
  ciepPlace,
  ocdPlace,
  synbirdPlace,
  wenHuaDaXuePlace
} from "./places"

// export const dinum = {
//   name: "Direction Interministérielle du Numérique",
//   place: "Paris",
//   description: "Oeuvre à l'accompagnement et à la réussite des projets numériques de l'Etat",
//   website: "https://www.numerique.gouv.fr/dinum/"
// }

// export const ricciInsitute = {
//   name: "Institut Ricci",
//   place: "Taipei, RdC",
//   description: "Promouvoir le dialogue inter-culturel sino-européen",
//   website: "http://www.riccibase.com",
// }

export const shanghaiFrenchConsulate = {
  name: "Consulat général de France",
  place: "Shanghai, RPC",
  description: "Représentation consulaire de la République française en République populaire de Chine",
  website: "https://cn.ambafrance.org/-Consulat-Shanghai-"
}

export const consulate = {
  title: "Chargé de mission auprès du Consul général adjoint",
  subtitle: "Porter assistance aux ressortissants français de la circonscription consulaire",
  places: [shanghaiFrenchConsulate],
  dates: "2009-2010",
  roles: [
    // {
    //   name: "Logistique",
    //   achievements: ["Coordination et communication du plan de vaccination H1N1 des français de l'étranger"]
    // }
  ]
}

export const teacher = {
  title: "Français langue étrangère",
  subtitle: "Participer à la diffusion de la culture française à l'étranger",
  places: [allianceFrancaisePlace, dongHuaDaXuePlace],
  dates: "2010-2012",
  roles: [
    // {
    //   name: "Enseignement",
    //   achievements: ["Cours à destination de groupes débutants (A1) à confirmés (C1), animation communautaire"]
    // }
  ]
}

export const mandarin = {
  title: "Etudes chinoises",
  subtitle: "Mieux comprendre la culture chinoise, apprendre une nouvelle langue",
  places: [wenHuaDaXuePlace],
  dates: "2010-2013",
  roles: [
    // {
    //   name: "Interculturel",
    //   achievements: [
    //     "Apprentissage autodidacte du mandarin (B1+), puis lauréat d'une bourse d'étude du gouvernement taïwanais"
    //   ]
    // }
  ]
}

export const ciep = {
  title: "Chargé de communication label Qualité français langue étrangère",
  subtitle: "Établissement public appuyant la diffusion de la langue française dans le monde",
  places: [ciepPlace],
  dates: "2013-2014",
  roles: [
    // {
    //   name: "Éditorial",
    //   achievements: [
    //     "Gestion de deux sites spécialisés lefildubilingue.org, qualitefle.fr, lettre d'information, animation communautaire"
    //   ]
    // },
    // {
    //   name: "Formation",
    //   achievements: [
    //     "Sensibilisation d'enseignants étrangers aux Technologies de l'information et de la communication dans l'enseignement"
    //   ]
    // }
  ]
}

export const cybersecurity = {
  title: "D'analyste cybercrime à ingénieur logiciel",
  subtitle: "Centre de réaction rapide spécialisé dans la lutte contre la cybercriminalité",
  dates: "2014-2021",
  places: [ocdPlace],
  roles: [
    {
      name: "Développement",
      achievements: [
        // "Amélioration de la stabilité et des performances du parc applicatif",
        "Gestion de legacy, stratégies de passage à l'échelle, transformation des pratiques (tests automatisés, pair programming)"
        // "Refonte en vue du passage à l'échelle d'un outil de veille cyber",
        // "Scripting, maintenance système"
      ]
    },
    {
      name: "Entrepreneuriat",
      achievements: [
        "Apprentissage autodidacte en ingénierie logicielle, design, implémentation et maintenance de deux outils internes SaaS"
        // "Fondation du Patrimoine : conception et réalisation d'un outil de détection de cagnottes frauduleuses : temps de traitement réduit de 95%",
      ]
    },
    {
      name: "Risques cyber",
      achievements: [
        "Investigations, recherches en sources ouvertes, plan de réaction cyber suite à l'incendie de Notre-Dame, 2019"
      ]
    },
    {
      name: "Publications",
      achievements: [
        // "« Usage des réseaux sociaux chinois dans un contexte professionnel », Rapport confidentiel, 2018",
        "« Les contrefaçons chinoises », Rapport confidentiel, 2017",
        "« La politique de surveillance en Chine Populaire », Sécurité & Défense Magazine, mars-mai 2016"
      ]
    }
  ]
}

export const synbird = {
  title: "Ingénieur produit",
  dates: "2020-2021",
  subtitle: "Plateforme SaaS de simplification de la relation entre usagers et collectivités",
  places: [synbirdPlace],
  roles: [
    {
      name: "Gestion de produit",
      achievements: ["Approche centrée utilisateur, support avancé, lancement du site démo du produit"]
    },
    {
      name: "DevOps",
      achievements: [
        "Mise en place de la supervision et de la chaîne de mise en production, stratégie de passage à l'échelle"
      ]
    },
    {
      name: "Fullstack",
      achievements: [
        "Amélioration de la qualité et de la sécurité, mise en conformité RGPD, montée en compétences Node, React, Pgsql"
      ]
    },
    // {
    //   name: "Sécurité",
    //   achievements: ["Audit de risques cyber, conception des Plan de reprise d’activité et Plan d’assurance sécurité"]
    // }
  ]
}

export const eig = {
  dates: "2022-2023",
  title: "Lauréat du programme Entrepreneurs d'intérêt général",
  subtitle: "Nubonyxia, plateforme interministérielle sécurisée d'outils data-science",
  places: [bercyHubPlace],
  roles: [
    {
      name: "Gestion de produit",
      achievements: ["Product discovery, approche par le design, définition et suivi des OKR, lancement de produit"]
    },
    {
      name: "Entrepreneuriat",
      achievements: [
        "Plan d'expérimentation de la version Bêta : 12 administrations au sein du MEFSIN, 6 projets déployés"
      ]
    },
    // {
    //   name: "DevOps",
    //   achievements: ["Implémentation de la supervision - Prometheus, Grafana, Alert Manager, dashboards métier"]
    // },
    {
      name: "Cloud",
      achievements: ["Gestion d'un cluster Kubernetes et de l'écosystème Onyxia, porté dans Nubo, cloud privé d'État"]
    }
  ]
}

export const openData = {
  dates: "2024 - Aujourd'hui",
  title: "Open Data Manager",
  subtitle: "Collecter, améliorer, valoriser les données ouvertes au sein du Ministère des Finances",
  places: [mefsin],
  roles: [
    {
      name: "Gestion de produit",
      achievements: ["Administration et développement de la plateforme open data ministérielle data.economie.gouv.fr"]
    },
    {
      name: "Gestion de projet",
      achievements: ["Pilotage, coordination et assistance à projets Open Data avec les directions du Ministère"]
    },
    {
      name: "Communauté",
      achievements: ["Animation, acculturation et formation à l'Open Data des agents MEFSIN"]
    },
  ]
}