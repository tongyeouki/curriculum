export const bercyHubPlace = {
  name: "Ministère des Finances",
  place: "Paris",
  description: "Aide les métiers du ministère à faire de leurs données autant d'actifs stratégiques",
  website: "https://www.bercynumerique.finances.gouv.fr/bercy-hub-sg-snum"
}

export const mefsin = {
  name: "Ministère des Finances",
  place: "Paris",
  description: "Aide les métiers du ministère à faire de leurs données autant d'actifs stratégiques",
  website: "https://www.data.economie.gouv.fr"
}

export const synbirdPlace = {
  name: "Synbird.com",
  place: "Chambéry",
  description: "Plateforme SaaS de gestion de rendez-vous pour les mairies et collectivités",
  website: "https://synbird.com"
}

export const ocdPlace = {
  name: "CERT Lexsi / Orange Cyberdefense",
  place: "Paris",
  description: "Centre de réaction rapide spécialisé dans la lutte contre la cybercriminalité",
  website: "https://www.orangecyberdefense.com/fr/services/detection-reponse/cert-orange-cyberdefense"
}

export const ciepPlace = {
  name: "France Éducation International",
  place: "Sèvres",
  description: "Oeuvre à l'appui de la diffusion de la langue française dans le monde",
  website: "https://www.france-education-international.fr"
}

export const allianceFrancaisePlace = {
  name: "Alliance Française",
  place: "Taipei & ",
  description: "Développer l'enseignement et l'usage de la langue française",
  website: "https://www.alliancefrancaise.org.tw/en/"
}

export const dongHuaDaXuePlace = {
  name: "National DongHwa University",
  place: "Hualien, RdC",
  description: "Développer l'enseignement et l'usage de la langue française",
  website: "https://www.ndhu.edu.tw/"
}

export const laGuildePlace = {
  name: "La Guilde & Ricci Institute",
  place: "Taipei, Hualien, RdC",
  description: "Rassemble des personnes engagées dans des projets d'intérêt général à l'étranger",
  website: "https://la-guilde.org/"
}

export const wenHuaDaXuePlace = {
  name: "Chinese Culture University",
  place: "Taipei, RdC",
  description: "Rassemble des personnes engagées dans des projets d'intérêt général à l'étranger",
  website: "https://mlc.sce.pccu.edu.tw/"
}
