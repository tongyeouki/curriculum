# Curriculum

Curriculum vitae en une page destiné à être imprimé en A4 au format pdf.

Également disponible [ici](https://paulmoysan.io/static/curriculum.pdf) au format pdf. 

## Installation

```bash
$ npm install
$ npm start
```

